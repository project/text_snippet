<?php

/**
 * @file
 * Features integration for text_snippet module.
 */

/**
 * Implements hook_features_export_options().
 */
function text_snippet_features_export_options() {
  $options = array();

  $text_snippets = text_snippet_load_multiple();

  foreach ($text_snippets as $text_snippet) {
    $options[$text_snippet->name] = $text_snippet->label . ' (' . $text_snippet->name . ')';
  }

  return $options;
}

/**
 * Implements hook_features_export().
 */
function text_snippet_features_export($data, &$export, $module_name) {
  // Add module dependencies.
  $export['dependencies']['entity'] = 'entity';
  $export['dependencies']['text_snippet'] = 'text_snippet';

  // The following is the simplest implementation of a straight object export
  // with no further export processors called.
  foreach ($data as $component) {
    $export['features']['text_snippet'][$component] = $component;
  }

  return $export;
}

/**
 * Implements hook_features_export_render().
 */
function text_snippet_features_export_render($module_name, $data, $export = NULL) {
  $code = array();

  foreach ($data as $name) {
    $text_snippet = text_snippet_load_by_name($name);

    $text_snippet_export = new StdClass();
    if ($text_snippet) {
      $text_snippet_export->label = $text_snippet->label;
      $text_snippet_export->name = $text_snippet->name;
      $text_snippet_export->description = $text_snippet->description;
      $text_snippet_export->block = $text_snippet->block;
      $text_snippet_export->pane = $text_snippet->pane;
      $text_snippet_export->token = $text_snippet->token;
    }

    $code[$name] = $text_snippet_export;
  }

  $code = '  return ' . features_var_export($code, '  ') . ';';

  return array('text_snippet_features_default_settings' => $code);
}

/**
 * Implements hook_features_rebuild().
 */
function text_snippet_features_rebuild($module) {
  $items = module_invoke($module, 'text_snippet_features_default_settings');

  // Loop over the items we need to recreate.
  foreach ($items as $name => $item) {
    // Basic function to take the array and store in the database.
    $text_snippet = text_snippet_load_by_name($name);
    if (!$text_snippet) {
      $text_snippet = entity_get_controller('text_snippet')->create();
    }

    $text_snippet->values = $item;

    text_snippet_save($text_snippet);
  }
}

/**
 * Implements hook_features_revert().
 */
function text_snippet_features_revert($module) {
  text_snippet_features_rebuild($module);
}
