<?php

/**
 * TextSnippetControllerInterface definition.
 *
 * We create an interface here because anyone could come along and
 * use hook_entity_info_alter() to change our controller class.
 * We want to let them know what methods our class needs in order
 * to function with the rest of the module, so here's a handy list.
 *
 * @see hook_entity_info_alter()
 */
interface TextSnippetControllerInterface extends DrupalEntityControllerInterface {

  /**
   * Create a text_snippet entity.
   */
  public function create();

  /**
   * Save a text_snippet entity.
   *
   * @param object $entity
   *   The entity to save.
   */
  public function save($entity);

  /**
   * Delete a text_snippet entity.
   *
   * @param object $entity
   *   The entity to delete.
   */
  public function delete($entity);
}

class TextSnippetController extends DrupalDefaultEntityController implements TextSnippetControllerInterface {
  /**
   * Create and return a new text_snippet entity.
   */
  public function create() {
    $entity = new stdClass();
    $entity->type = 'text_snippet';
    $entity->tsid = 0;
    $entity->language = LANGUAGE_NONE;

    return $entity;
  }

  /**
   * Saves the custom fields using drupal_write_record().
   */
  public function save($entity) {
    // There is only one bundle.
    $entity->bundle = 'text_snippet';

    // If our entity has no id, then we need to give it a time of creation.
    if (empty($entity->tsid)) {
      $entity->created = time();
    }

    if (isset($entity->values)) {
      $entity->label = $entity->values['label'];
      $entity->name = $entity->values['name'];
      $entity->description = $entity->values['description'];
      $entity->block = $entity->values['block'];
      $entity->pane = $entity->values['pane'];
      $entity->token = $entity->values['token'];
    }

    // Invoke hook_entity_presave().
    module_invoke_all('entity_presave', $entity, 'text_snippet');
    // The 'primary_keys' argument determines whether this will be an insert or
    // an update. So if the entity already has an ID, we'll specify tsid as the
    // key.
    $primary_keys = $entity->tsid ? 'tsid' : array();
    // Write out the entity record.
    drupal_write_record('text_snippet', $entity, $primary_keys);
    // We're going to invoke either hook_entity_update() or
    // hook_entity_insert(), depending on whether or not this is a new entity.
    // We'll just store the name of hook_entity_insert() and change it if we
    // need to.
    $invocation = 'entity_insert';
    // Now we need to either insert or update the fields which are attached to
    // this entity. We use the same primary_keys logic to determine whether to
    // update or insert, and which hook we need to invoke.
    if (empty($primary_keys)) {
      field_attach_insert('text_snippet', $entity);
    }
    else {
      field_attach_update('text_snippet', $entity);
      $invocation = 'entity_update';
    }
    // Invoke either hook_entity_update() or hook_entity_insert().
    module_invoke_all($invocation, $entity, 'text_snippet');

    return $entity;
  }

  /**
   * Delete a single text_snippet entity.
   *
   * A convenience function for deleteMultiple().
   */
  public function delete($entity) {
    $this->deleteMultiple(array($entity));
  }

  /**
   * Delete one or more text entities.
   *
   * Deletion is unfortunately not supported in the base
   * DrupalDefaultEntityController class.
   *
   * @param array $entities
   *   An array of entity IDs or a single numeric ID.
   */
  public function deleteMultiple($entities) {
    $tsids = array();
    if (!empty($entities)) {
      $transaction = db_transaction();
      try {
        foreach ($entities as $entity) {
          // Invoke hook_entity_delete().
          module_invoke_all('entity_delete', $entity, 'text_snippet');
          field_attach_delete('text_snippet', $entity);
          $tsids[] = $entity->tsid;
        }
        db_delete('text_snippet')
          ->condition('tsid', $tsids, 'IN')
          ->execute();
      }
      catch (Exception $e) {
        $transaction->rollback();
        watchdog_exception('text_snippet', $e);
        throw $e;
      }
    }
  }

  public function attachLoad(&$entities, $revision_id = FALSE) {
    // The text_snippet entity is designed to be not fieldable, but it still gets a
    // default field, so we simulate this during attachLoad().
    $this->entityInfo['fieldable'] = TRUE;
    parent::attachLoad($entities, $revision_id);
    $this->entityInfo['fieldable'] = FALSE;
  }

}
