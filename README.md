# Text snippet Drupal module

## Summary

Text snippet is a simple module aiming at solving a very common task - creating
small (static) text chunks, which can be placed in a site via the block system,
as a content pane or even as a token. Typical use-case would be if you need to
provide a block with text which should be placed in the footer of your site.
Your site-editor would only need to edit their texts on the text snippet UI
page.

Text snippets are exportable entities, so their configuration can be stored in
code. The good thing is that the content of a text snippet is provided via the
Field API (every text snippet entity gets a single long_text field), so this
content remains content, which can be changed by site editors, without having
to re-export the feature, which configures its' placement.

The (possibly) most important thing is that as entities text snippets have
support for translation via the Entity translation module. So as a site
administrator you actually only need to define where and how the text snippet
is placed, content and translating is a task of your site editors.

## Installation and usage

Download and place the module in your modules directory. After enabling it you
can find the configuration page at /admin/content/text-snippet.

You can create several text-snippets and configure them to appear either as
block, as a content pane or as a token.

To add the actual content of the snippet click on it's title on the text
snippet overview page.

If entity_translation module is enabled and text_snippet is enabled for
translating you'll get the language field on the content edit page.
