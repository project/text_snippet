<?php

/**
 * @file
 * Provides info about the node entity.
 */

/**
 * Implements hook_entity_property_info().
 *
 * @see entity_entity_property_info()
 */
function text_snippet_entity_property_info() {
  $info = array();
  $properties = &$info['text_snippet']['properties'];

  $default_properties = array(
    'setter callback' => 'entity_property_verbatim_set',
  );

  $properties['language'] = array(
      'label' => t('Language'),
      'type' => 'token',
      'description' => t('The language the entity is written in.'),
      'options list' => 'entity_metadata_language_list',
      'schema field' => 'language',
      'setter permission' => 'administer text snippets',
    ) + $default_properties;

  return $info;
}
