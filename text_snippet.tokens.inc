<?php

/**
 * @file
 * Tokens definitions for the text_snippet module.
 */

/**
 * Implements hook_token_info().
 */
function text_snippet_token_info() {
  $text_snippets = text_snippet_load_by_setting('token');

  if ($text_snippets) {
    $info['types']['text_snippet'] = array(
      'name' => t('Text snippet'),
      'description' => t('Text snippet entities'),
    );

    foreach ($text_snippets as $text_snippet_props) {
      $text_snippet = text_snippet_load($text_snippet_props->tsid);
      $info['tokens']['text_snippet'][$text_snippet->name] = array(
        'name' => $text_snippet->label,
        'description' => $text_snippet->description,
      );
    }

    return $info;
  }
}

/**
 * Implements hook_tokens().
 */
function text_snippet_tokens($type, $tokens, array $data = array(), array $options = array()) {
  $replacements = array();

  if ($type == 'text_snippet') {
    // Loop through the available tokens.
    foreach ($tokens as $name => $original) {
      if ($text_snippet = text_snippet_load_by_name($name)) {
        $replacements[$original] = text_snippet_get_content($text_snippet);
      }
    }
  }

  return $replacements;
}
