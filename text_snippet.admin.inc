<?php

/**
 * @file
 * Admin functions for the text_snippet module.
 */

/**
 * Provides a wrapper on the edit form to add a new text snippet entity.
 */
function text_snippet_add() {
  // Create a basic text snippet entity structure to be used and passed to the
  // validation and submission functions.
  $entity = entity_get_controller('text_snippet')->create();
  drupal_set_title(t('Add !type', array('!type' => $entity->type)));
  return drupal_get_form('text_snippet_edit_form', $entity);
}

/**
 * Provides a wrapper on the edit form to edit a text snippet entity.
 */
function text_snippet_edit($entity, $op = 'edit_config') {
  // Load a text snippet entity structure to be used and passed to the
  // validation and submission functions.
  drupal_set_title(t('Edit !bundle !label', array('!bundle' => $entity->bundle, '!label' => $entity->label)));
  return drupal_get_form('text_snippet_edit_form', $entity, $op);
}

/**
 * The add/edit form for creating/editing a text snippet entity.
 */
function text_snippet_edit_form($form, &$form_state, $entity, $op = 'add') {
  // During initial form build, add the text_snipept entity to the form state
  // for use during form building and processing. During a rebuild, use what is
  // in the form state.
  if (!isset($form_state['text_snippet'])) {
    $form_state['text_snippet'] = $entity;
  }
  else {
    $entity = $form_state['text_snippet'];
  }

  if ($op != 'edit_content') {
    $form['label'] = array(
      '#type' => 'textfield',
      '#title' => t('Label'),
      '#description' => t('Add a label for the text snippet.'),
      '#required' => TRUE,
      '#size' => 40,
      '#maxlength' => 127,
      '#default_value' => isset($entity->label) ? $entity->label : '',
    );

    $form['name'] = array(
      '#type' => 'machine_name',
      '#title' => t('Text snippet machine name'),
      '#description' => t('Add a unique machine name for this text snippet.'),
      '#default_value' => isset($entity->name) ? $entity->name : '',
      '#required' => TRUE,
      '#machine_name' => array(
        'exists' => 'text_snippet_machine_name_exists',
        'source' => array('label'),
      ),
      '#disabled' => isset($entity->name) ? TRUE : FALSE,
    );

    $form['description'] = array(
      '#type' => 'textarea',
      '#title' => t('Text snippet description'),
      '#description' => t('Add a description for this text snippet. It will be available on the form where the text snippets\' content field is being displayed.'),
      '#required' => TRUE,
      '#default_value' => isset($entity->description) ? $entity->description : '',
      '#rows' => 2,
    );

    $form['block'] = array(
      '#title' => t('Available as block'),
      '#description' => t('Make the value of this text snippet be available as a block.'),
      '#type' => 'checkbox',
      '#default_value' => isset($entity->block) ? $entity->block : '',
    );

    $form['pane'] = array(
      '#title' => t('Available as content pane'),
      '#description' => t('Make the value of this text snippet be available as a content pane.'),
      '#type' => 'checkbox',
      '#default_value' => isset($entity->pane) ? $entity->pane : '',
    );

    $form['token'] = array(
      '#title' => t('Available as token'),
      '#description' => t('Make the value of this text snippet be available as token.'),
      '#type' => 'checkbox',
      '#default_value' => isset($entity->token) ? $entity->token : '',
    );
  }
  else {
    $language = LANGUAGE_NONE;
    if (function_exists('entity_language')) {
      // entity_language() was added in Drupal 7.15.
      $language = entity_language('text_snippet', $entity);
    }

    $form['language'] = array(
      '#type' => 'value',
      '#value' => $language,
    );

    field_attach_form('text_snippet', $entity, $form, $form_state, $language);
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#weight' => 50,
    '#op' => $op,
  );

  if ($op == 'edit_config') {
    $form['submit']['#value'] = t('Update');
  }
  else if ($op == 'edit_content') {
    $form['submit']['#value'] = t('Save');
  }
  else {
    $form['submit']['#value'] = t('Add');
  }

  $form['cancel'] = array(
    '#markup' => l(t('Cancel'), 'admin/content/text-snippet'),
    '#weight' => 51,
  );

  return $form;
}

/**
 * Validation handler for text_snippet_edit_form form.
 *
 * We pass things straight through to the Field API to handle validation
 * of the attached fields.
 */
function text_snippet_edit_form_validate($form, &$form_state) {
  if ($form_state['values']['submit'] == 'Save') {
    field_attach_form_validate('text_snippet', $form_state['text_snippet'], $form, $form_state);
  }
}

/**
 * Form submit handler: Submits text information.
 */
function text_snippet_edit_form_submit($form, &$form_state) {
  $entity = $form_state['text_snippet'];

  if ($form_state['clicked_button']['#op'] != 'edit_content') {
    $values = $form_state['values'];
    $entity->values = $values;

    $clear_cache_token = FALSE;

    // Entity is new
    if (!$entity->tsid) {
      // Clear token cache when token checkbox value is initially checked.
      $clear_cache_token = $values['token'] ? TRUE : FALSE;
    }
    else {
      // Clear token cache every time token checkbox value changes.
      $clear_cache_token = ($entity->token != $values['token']) ? TRUE : FALSE;
    }

    if ($clear_cache_token) {
      cache_clear_all('*', 'cache_token', TRUE);
    }

    $message = t('Text snippet configuration saved.');
  }
  else {
    $entity->language = $form_state['values']['language'];
    field_attach_submit('text_snippet', $entity, $form, $form_state);
    $message = t('Text snippet content saved.');
  }

  drupal_set_message($message);
  text_snippet_save($entity);

  $form_state['redirect'] = 'admin/content/text-snippet';
}

/**
 * Provides a confirmation form to delete a text snippet entity.
 */
function text_snippet_delete_form($form, &$form_state, $entity) {
  $question = t('You are about to delete text snippet %text_snippet_label. Are you sure?', array('%text_snippet_label' => $entity->label));
  $path = 'admin/content/text-snippet';

  $form_state['storage']['entity'] = $entity;

  return confirm_form(
    $form,
    $question,
    $path
  );
}

/**
 * Provides a confirmation form to delete a text snippet entity.
 */
function text_snippet_delete_form_submit($form, &$form_state) {
  $entity = $form_state['storage']['entity'];

  if ($entity->token) {
    cache_clear_all('*', 'cache_token', TRUE);
  }

  text_snippet_delete($entity);

  $message = t('Deleted text snippet %text_snippet_label', array('%text_snippet_label' => $entity->label));
  drupal_set_message($message);
  drupal_goto('admin/content/text-snippet');
}

/**
 * Form settings for text snippet settings.
 */
function text_snippet_settings_form($form, &$form_state) {
  $form = array();

  $form['text_snippet_permission_per_snippet'] = array(
    '#title' => t('Permission per snippet'),
    '#description' => t('Create a separate permission for every snippet entity. Useful if you want to allow access for text snippet per role. NOT IMPLEMENTED YET.'),
    '#type' => 'checkbox',
    '#default_value' => variable_get("text_snippet_permission_per_snippet", 0),
    '#disabled' => TRUE,
  );

  return $form;

  // Change to this as soon as text_snippet_permission_per_snippet is
  // implemented.
  //return system_settings_form($form);
}
