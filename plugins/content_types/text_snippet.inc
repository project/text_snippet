<?php

/**
 * @file
 * Handle rendering of a text snippet pane.
 */

$panes = text_snippet_load_by_setting('pane');

if ($panes) {
  $plugin = array(
    'title' => t('Text snippet'),
    'defaults' => array(
      'text_snippet_name' => '',
    ),
    'category' => t('Text snippets'),
    'render callback' => 'text_snippet_content_type_render',
    'admin title' => 'text_snippet_content_type_admin_title',
    'admin info' => 'text_snippet_content_type_admin_info',
    'edit form' => 'text_snippet_content_type_edit_form',
    'all contexts' => TRUE,
    'icon' => 'text_snippet.png',
  );
}

/**
 * Render the custom content type.
 */
function text_snippet_content_type_render($subtype, $config, $arguments, &$contexts) {
  $text_snippet_name = $config['text_snippet_name'];

  if (!$text_snippet_name) {
    return;
  }

  $text_snippet = text_snippet_load_by_name($text_snippet_name);

  if (!$text_snippet->pane) {
    $message = t('Text snippet %text_snippet is not configured to be used in a pane. Please contact your site administrator.', array('%text_snippet' => $text_snippet->label));
    $content = t($message);
  }
  else {
    $content = text_snippet_get_content($text_snippet);
  }

  $block = new stdClass();
  $block->title = $text_snippet->label;
  $block->content = $content;

  return $block;
}

/**
 * Returns the administrative title for an instance of this content type.
 */
function text_snippet_content_type_admin_title($subtype, $conf, $context) {
  return t('Text snippet content');
}

/**
 * Returns the administrative information for an instance of this content type.
 */
function text_snippet_content_type_admin_info($subtype, $conf, $context) {
  $text_snippet_name = $conf['text_snippet_name'];

  if (!$text_snippet_name) {
    return;
  }

  $text_snippet = text_snippet_load_by_name($text_snippet_name);

  if ($text_snippet->pane) {
    $title = t('Content of text snippet %text_label', array('%text_label' => $text_snippet->label));
  }
  else {
    $title = t('Text snippet %text_label (NOT ENABLED!)', array('%text_label' => $text_snippet->label));
  }

  return (object) array(
    'title' => $title,
    'content' => $text_snippet->description,
  );
}

/**
 * Allow some detailed configuration.
 */
function text_snippet_content_type_edit_form($form, &$form_state) {
  $conf = $form_state['conf'] + $form_state['plugin']['defaults'];

  $text_snippets = text_snippet_load_by_setting('pane');

  $text_snippet_options = array();
  foreach ($text_snippets as $text_snippet_props) {
    $text_snippet = text_snippet_load($text_snippet_props->tsid);
    $text_snippet_options[$text_snippet->name] = $text_snippet->label;
  }

  $form['text_snippet_name'] = array(
    '#type' => 'select',
    '#title' => t('Text snippet'),
    '#description' => t('Please select a text snippet'),
    '#options' => $text_snippet_options,
    '#required' => TRUE,
    '#default_value' => $conf['text_snippet_name'],
  );

  return $form;
}

/**
 * Save configuration.
 */
function text_snippet_content_type_edit_form_submit($form, &$form_state) {
  foreach ($form_state['plugin']['defaults'] as $key => $value) {
    if (isset($form_state['values'][$key])) {
      $form_state['conf'][$key] = $form_state['values'][$key];
    }
  }

  return $form;
}
