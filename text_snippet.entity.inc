<?php

/**
 * @file
 * Entity definitions for the text_snippet module.
 */

/**
 * Implements hook_entity_info().
 */
function text_snippet_entity_info() {
  $info = array();

  $info['text_snippet'] = array(
    'label' => t('Text snippet'),
    'base table' => 'text_snippet',
    'entity keys' => array(
      'id' => 'tsid',
      'label' => 'label',
      'bundle' => 'bundle',
      'language' => 'language',
    ),
    'bundle keys' => array(
      'bundle' => 'bundle',
    ),
    'module' => 'text_snippet',
    'controller class' => 'TextSnippetController',
    'static cache' => TRUE,
    'bundles' => array(
      'text_snippet' => array(
        'label' => t('Text snippet'),
        'admin' => array(
          'path' => 'admin/content/text-snippet',
          'real path' => 'admin/content/text-snippet',
          'access arguments' => array('administer text snippets'),
        ),
      )
    ),
    'translation' => array(
      'entity_translation' => array(
        'class' => 'EntityTranslationTextSnippetHandler',
        'base path' =>  'admin/content/text-snippet/%text_snippet',
        'edit path' =>  'admin/content/text-snippet/%text_snippet/edit',
        'path wildcard' => '%text_snippet',
      ),
    ),
  );

  if (module_exists('entity_translation')) {
    $info['text_snippet']['fieldable'] = TRUE;
  }

  return $info;
}
